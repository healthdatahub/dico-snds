#!/usr/bin/env bash
set -euo pipefail

# Install system dependencies
apt-get update && apt-get install -y \
    libv8-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    xclip \
    libsasl2-dev \
    zlib1g-dev \
    curl \
    libfontconfig1-dev \
    libxml2-dev \
    libglpk-dev

# Configure R options
r -e 'options(
    repos = c(CRAN = "https://cloud.r-project.org"),
    pkgType = "binary",
    install.packages.compile.from.source = "never"
)'

# Install all required packages with dependencies
r -e "install.packages(c(
    'backports', 'crosstalk', 'clipr', 'dplyr', 'DT', 'elastic', 'evaluate',
    'ggplot2', 'gridExtra', 'highr', 'knitr', 'markdown', 'networkD3', 
    'rmarkdown', 'rprojroot', 'rsconnect', 'shiny', 'testthat', 'tidyr', 
    'igraph', 'R6', 'Rcpp', 'brio', 'bslib', 'cachem', 'cli', 'colorspace', 
    'commonmark', 'cpp11', 'crayon', 'crul', 'curl', 'digest', 'evaluate',
    'farver', 'fastmap', 'fontawesome', 'fs', 'xfun', 'yaml'
), dependencies = TRUE)"

# Set up rsconnect credentials
r -e "rsconnect::setAccountInfo(name='health-data-hub', token='${TOKEN}', secret='${SECRET}')"

# Configure environment variables
echo "ES_PWD=${ES_PWD}" >> app/server/var_env.txt
echo "ES_HOST=${ES_HOST}" >> app/server/var_env.txt
echo "ES_USERNAME=${ES_USERNAME}" >> app/server/var_env.txt
echo "ES_PORT=${ES_PORT}" >> app/server/var_env.txt

# Deploy the application
echo "deploy app"
r -e "rsconnect::deployApp(
    appDir = 'app',
    appName = 'dico-snds-test',
    account = '${ACCOUNT}',
    server = '${SERVER}',
    forceUpdate = TRUE
)"

# Run test suite
echo 'deployment to shinyapps.io done !'
sleep 30s
echo 'curl to shinyapps.io/dico-snds-test'
curl "https://health-data-hub.shinyapps.io/dico-snds-test/" > app/dico.html
cd app/tests/
echo 'run tests'
Rscript backend_tests.R > output.txt
echo 'return errors'
cat output.txt
echo 'end of errors'
r -e "stopifnot(sum(grepl('(error-code)|(error has occurred)',readLines('../dico.html')))==0)"
cd ../../